import {Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, JoinColumn, OneToMany, ManyToOne} from "typeorm";
import {Ingredient} from "./Ingredient";
import {Step} from "./Step";
import {Category} from "./Category";
import {User} from "../auth/User";

@Entity()
export class Recipe {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToOne(type => User, author => author.recipes)
    author: User;

    @ManyToMany(type => Ingredient)
    @JoinTable()
    ingredients: Ingredient[];

    @ManyToMany(type => Step)
    @JoinTable()
    steps: Step[];

    @ManyToOne(type => Category, category => category.recipes)
    category: Category;
}
