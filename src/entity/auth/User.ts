import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import {Recipe} from "../recipe/Recipe";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    birthday: string;

    @Column()
    password: string;

    @Column()
    email: string;

    @Column()
    validPwd: boolean;

    @OneToMany(type => Recipe, recipe => recipe.author)
    recipes: Recipe[];

    token: string;

}
