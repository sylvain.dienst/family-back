import {Request, Response} from "express";
import {getConnection, getRepository} from "typeorm";
import {User} from "../entity/auth/User";
import * as jwt from "jsonwebtoken"

export const  login = async (req: Request, res: Response) => {
    try {
        const user : User = await  getConnection(process.env.NODE_ENV).getRepository(User)
            .createQueryBuilder("user")
            .where("user.email = :email", {email: req.body.email})
            .getOne();
        if (user) {
            user.token = jwt.sign({
                sub: user.id,
                exp: Math.floor(Date.now() / 1000) + (60 * 60 * 24),
            }, 'y4oW5W&Zb');
            res.json(user).end()
        } else {
            res.status(400).send({message: 'User not found'})
        }
    } catch (e) {
        res.status(500).send({message: e});
    }
};

export const changePassword = async (req: Request, res: Response) => {
    try {
        const user : User =  await getConnection(process.env.NODE_ENV).getRepository(User).findOne(req.body.id);
        user.password = req.body.pwd1;
        user.validPwd = true;
        await getConnection(process.env.NODE_ENV).getRepository(User).save(user);
        res.status(200).send()
    } catch (e){
        res.status(500).send({message: e})
    }
}
