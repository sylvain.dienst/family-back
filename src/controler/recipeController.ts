import {Request, Response} from "express";
import {getConnection, getRepository} from "typeorm";
import {Recipe} from "../entity/recipe/Recipe";
import {repository} from "../helpers/ormRepositoryHandler"
import {handleError} from "../helpers/errorsHandler";

const option = {relations: ["category", "ingredients", "steps", "ingredients.unitQuantity"]};

export const findAll = async (req: Request, res: Response) => {
    try {
        const recipes: Recipe[] = await repository(req, res, Recipe).find(option);
        return res.send(recipes);
    } catch(e) {
        return res.status(handleError(e).status).send(handleError(e).message)
    }
};

export const find = async (req: Request, res: Response) => {
    try {
        const recipe: Recipe = await repository(req, res, Recipe).findOneOrFail(req.params.id, option);
        return res.send(recipe)
    } catch(e) {
        return res.status(handleError(e).status).send(handleError(e).message)
    }
};

export const create = async (req: Request, res: Response) => {
    try {
        const repo = await repository(req, res, Recipe);
        const results = repo.save(repo.create(req.body));
        return res.send(results);
    } catch(e) {
        return res.status(handleError(e).status).send(handleError(e).message)
    }
};

export const update = async (req: Request, res: Response) => {
    try {
        const repo = await repository(req, res, Recipe);
        const recipe = await repo.findOne(req.params.id);
        repo.merge(recipe, req.body);
        const results = await repo.save(recipe);
        return res.send(results);
    } catch(e) {
        return res.status(handleError(e).status).send(handleError(e).message)
    }
};

export const deleteRecipe = async (req: Request, res: Response) => {
    try {
        const repo = await repository(req, res, Recipe);
        const results = await repo.delete(req.params.id);
        return res.send(results);
    } catch(e) {
        return res.status(handleError(e).status).send(handleError(e).message)
    }
};