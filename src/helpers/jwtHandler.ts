import * as jwt from "jsonwebtoken"
import {Request, Response} from "express";
import {UnauthorizedError} from "../exception/UnauthorizedError";

export default (req: Request, res: Response): void => {
    try {
        jwt.verify(req.header('Authorization'), 'y4oW5W&Zb')
    } catch {
        throw new UnauthorizedError()
    }
}