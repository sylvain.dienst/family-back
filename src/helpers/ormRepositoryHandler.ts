import {getConnection, Repository} from "typeorm";
import {Request, Response} from "express";
import verifyAuth from "./jwtHandler";
import {UnauthorizedError} from "../exception/UnauthorizedError";
import {ConnectionError} from "../exception/ConnectionError";

export const repository = (req: Request, res: Response, entity: any): Repository<any> => {
    try {
        verifyAuth(req, res);
        return getConnection(process.env.NODE_ENV).getRepository(entity);
    } catch (e){
        if (e instanceof UnauthorizedError) {
            throw e;
        } else {
            throw new ConnectionError();
        }
    }
};